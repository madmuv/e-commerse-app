package xyz.madmuv.ecommerse101.cart

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import xyz.madmuv.ecommerse101.R

class CartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
    }
}
