package xyz.madmuv.ecommerse101.repos

import xyz.madmuv.ecommerse101.model.Product
import retrofit2.http.GET

interface EcommerceApi {

    @GET("api/ecommerce/v1/allProducts")
    suspend fun fetchAllProducts(): List<Product>
}
